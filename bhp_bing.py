#!/usr/bin/env python3

from burp import IBurpExtender
from burp import IContextMenuFactory

from javax.swing import JMenuItem
from java.util import List, ArrayList
from java.net import URL

import base64
import json
import re
import socket
import textwrap
import urllib.request

bing_api_key = "KEY"

class BurpExtender(IBurpExtender, IContextMenuFactory):
    def registerExtenderCallbacks(self, callbacks):
        self._callbacks = callbacks
        self._helpers = callbacks.getHelpers()
        self.context = None

        # we set up our extension
        callbacks.setExtensionName("BHP Bing")
        callbacks.registerContextMenuFactory(self)

        return

    def createMenuItems(self, context_menu):
        self.context = context_menu
        menu_list = ArrayList()
        menu_list.add(JMenuItem("Send to Bing", actionPerformed = self.bing_menu))

        return menu_list

    def bing_menu(self, event):
        # grab the details of what the user clicked
        http_traffic = self.context.getSelectedMessages()

        print(f"{len(http_traffic)} request highlighted")

        for traffic in http_traffic:
            http_service = traffic.getHttpService()
            host = http_service.getHost()

            print(f"User selected host: {host}")

            self.bing_search(host)

        return

    def bing_search(self, host):
        # check if we have an IP or hostname
        is_ip = re.match("[0-9]+(?:\.[0-9]+){3}", host)

        if is_ip:
            ip_address = host
            domain = False
        else:
            ip_address = socket.gethostbyname(host)
            domain = True

        bing_query_string = "'ip:{ip_address}'"
        self.bing_query(bing_query_string)

        if domain:
            bing_query_string = "'domain:{host}'"
            self.bing_query(bing_query_string)

        return

    def bing_query(self, bing_query_string):
        print(f"Performing Bing search: {bing_query_string}")

        # encode our query
        quoted_query = urllib.request.quote(bing_query_string)

        http_request = textwrap.dedent(
            f"""\
            GET https://api.datamarket.azure.com/Bing/Search/Web?$format=json&$top=20&Query={quoted_query} HTTP/1.1\r\n
            Host: api.datamarket.azure.com\r\n
            Connection: close\r\n
            Authorization: Basic {base64.b64decode(f':{bing_api_key}')}\r\n
            User-Agent: Blackhat Python\r\n\r\n\
            """)

        json_body = self._callbacks.makeHttpRequest("api.datamarket.azure.com", 443, True, http_request).toString()

        json_body = json_body.split("\r\n\r\n", 1)[1]

        try:
            r = json.loads(json_body)

            if len(r["d"]["results"]):
                for site in r["d"]["results"]:
                    print(textwrap.dedent(
                        f"""\
                        {'*' * 100}
                        {site['Title']}
                        {site['Url']}
                        {site['Description']}
                        {'*' * 100}\
                        """)

                    j_url = URL(site['Url'])

                    if not self._callbacks.isInScope(j_url):
                        print("Adding to Burp scope")
                        self._callbacks.includeInScope(j_url)
        except:
            print("No results from Bing")
            pass

        return
